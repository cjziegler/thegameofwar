﻿using TheGameOfWar.Cards;

namespace TheGameOfWar.Game
{
    public class PlayedCard
    {
        public int PlayerId { get; set; }

        public Card Card { get; set; }
    }
}
