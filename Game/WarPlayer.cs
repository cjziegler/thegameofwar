﻿using System;
using System.Collections.Generic;
using System.Linq;
using TheGameOfWar.Cards;

namespace TheGameOfWar.Game
{
    public class WarPlayer
    {
        public static WarResult PlayWar(IEnumerable<Player> players)
        {
            Console.WriteLine("-----------------------------");
            Console.WriteLine("Playing a round of War\r\n");

            var allCards = new Deck();
            var finalCards = new List<PlayedCard>();

            foreach(Player player in players)
            {
                var cardsToDraw = 3;
                Console.Write("Drawing Cards: ");
                while(cardsToDraw > 0 && !player.Deck.IsEmpty)
                {
                    Card drawnCard = player.Deck.Draw();
                    Console.Write($"{drawnCard.ToShortString()} ");
                    allCards.AddCard(drawnCard);

                    cardsToDraw--;
                }

                Console.WriteLine();
                if (!player.Deck.IsEmpty)
                {
                    Card drawnCard = player.Deck.Draw();
                    Console.WriteLine($"Player {player.ID} War card is: {drawnCard} ");
                    allCards.AddCard(drawnCard);
                    finalCards.Add(new PlayedCard { PlayerId = player.ID, Card = drawnCard });
                }
                else
                    Console.WriteLine($"Player {player.ID} is out of cards!");

                Console.WriteLine();
            }

            PlayedCard winningCard = finalCards.OrderByDescending(pc => pc.Card).First();
            int winnerId = winningCard.PlayerId;

            List<PlayedCard> warCards = GetWarCards(finalCards, winningCard.Card);
            if (warCards.Count > 1)
            {
                WarResult result = PlayWar(players.Where(p => warCards.Any(c => c.PlayerId == p.ID)));

                result.DeckWon.Shuffle().PutInto(allCards);

                winnerId = result.WinnerId;
            }

            Console.WriteLine("-----------------------------");

            return new WarResult { WinnerId = winnerId, DeckWon = allCards };
        }

        public static List<PlayedCard> GetWarCards(IEnumerable<PlayedCard> playedCards, Card winningCard)
        {
            return playedCards.Where(c => c.Card.Value == winningCard.Value).ToList();
        }
    }

    public class WarResult
    {
        public int WinnerId { get; set; }

        public Deck DeckWon { get; set; }
    }
}
