﻿using System.Collections.Generic;
using System.Linq;
using TheGameOfWar.Cards;

namespace TheGameOfWar.Game
{
    public class PlayerFactory
    {
        public static List<Player> InitializePlayers(int numberOfPlayers)
        {
            var players = new List<Player>();

            List<Deck> decks = Deck.CreateStartingDeck().Shuffle().SplitInto(numberOfPlayers);

            foreach (int index in Enumerable.Range(1, numberOfPlayers))
               players.Add(new Player { ID = index, Deck = decks[index - 1].Shuffle() });

            return players;
        }
    }
}
