﻿using TheGameOfWar.Cards;

namespace TheGameOfWar.Game
{
    public class Player
    {
        public int ID { get; set; }
        public Deck Deck { get; set; }
    }
}
