﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TheGameOfWar.Cards;

namespace TheGameOfWar.Game
{
    public class GameController
    {
        private const int MinNumberOfPlayers = 2;
        private const int MaxNumberOfPlayers = 8;

        private List<Player> players;

        public GameController()
        {
            players = new List<Player>();
        }

        public void Play()
        {
            int numberOfPlayers = GetNumberOfPlayers();

            players = PlayerFactory.InitializePlayers(numberOfPlayers);

            var auto = false;
            while (!IsEndOfGame())
            {
                if (auto)
                    Thread.Sleep(0500);
                else
                {
                    Console.WriteLine("To play a round, hit any key or type A(uto) to playout the rest of the game:");
                    char input = Console.ReadKey().KeyChar;
                    if (input == 'A' || input == 'a')
                        auto = true;
                }

                Console.WriteLine("\r\n");
                Console.WriteLine("=============================");
                PlayTurn();
                Console.WriteLine("=============================");
            }

            EndGame();

            Console.ReadKey();
        }

        private static int GetNumberOfPlayers()
        {
            var numberOfPlayers = 0;
            do
            {
                Console.Write("How many players are in this game? (2-8) ");

                if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out numberOfPlayers)
                    && numberOfPlayers <= MaxNumberOfPlayers
                    && numberOfPlayers >= MinNumberOfPlayers)
                    break;

                Console.WriteLine("\r\nPlease enter a number between 2 and 8.");
            } while (true);

            Console.WriteLine("\r\n\r\n");

            return numberOfPlayers;
        }

        private bool IsEndOfGame()
        {
            return players.Count(p => !p.Deck.IsEmpty) == 1;
        }

        private void PlayTurn()
        {
            var turnCards = new List<PlayedCard>();

            List<Player> activePlayers = players.Where(p => !p.Deck.IsEmpty).ToList();
            foreach (Player player in activePlayers)
            {
                Card drawnCard = player.Deck.Draw();
                Console.WriteLine($"Player {player.ID} has drawn the {drawnCard}");

                turnCards.Add(new PlayedCard { PlayerId = player.ID, Card = drawnCard });
            }

            PlayedCard winningCard = turnCards.OrderByDescending(pc => pc.Card).First();

            // Determine if we need to play war
            List<PlayedCard> warCards = WarPlayer.GetWarCards(turnCards, winningCard.Card);
            if (warCards.Count > 1)
            {
                WarResult result = WarPlayer.PlayWar(players.Where(p => warCards.Any(c => c.PlayerId == p.ID)));

                result.DeckWon.Shuffle().PutInto(players.First(p => p.ID == result.WinnerId).Deck);

                winningCard.PlayerId = result.WinnerId;
            }

            new Deck().AddCards(turnCards.Select(playerCard => playerCard.Card)).Shuffle()
                .PutInto(players.First(p => p.ID == winningCard.PlayerId).Deck);

            Console.WriteLine($"\r\nPlayer {winningCard.PlayerId} has won the round!");

            foreach (Player outPlayer in activePlayers.Except(activePlayers.Where(p => !p.Deck.IsEmpty)))
                Console.WriteLine($"\r\nPlayer {outPlayer.ID} is out of the game!");
        }

        private void EndGame()
        {
            Player player = players.First(p => !p.Deck.IsEmpty);

            Console.WriteLine($"Player {player.ID} has won the game!");
        }
    }
}
