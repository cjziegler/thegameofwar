﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheGameOfWar.Cards
{
    public class Deck
    {
        private List<Card> cards;

        public Deck()
        {
            cards = new List<Card>();
        }

        private Deck Initialize()
        {
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                foreach (CardValue value in Enum.GetValues(typeof(CardValue)))
                    cards.Add(new Card { Suit = suit, Value = value });
            }

            return this;
        }

        public static Deck CreateStartingDeck()
        {
            return new Deck().Initialize();
        }

        public bool IsEmpty => !cards.Any();

        public Card Draw()
        {
            if (IsEmpty)
                return null;

            Card card = cards[0];
            cards.RemoveAt(0);

            return card;
        }

        public Deck Shuffle()
        {
            cards = cards?.OrderBy(c => Guid.NewGuid()).ToList();

            return this;
        }

        public Deck AddCard(Card card)
        {
            cards.Add(card);

            return this;
        }

        public Deck AddCards(IEnumerable<Card> cardCollection)
        {
            this.cards.AddRange(cardCollection);

            return this;
        }

        /// <summary>
        /// This will split the current deck into new decks, the amount of times provided
        /// </summary>
        /// <param name="numberOfDecks"></param>
        /// <returns></returns>
        public List<Deck> SplitInto(int numberOfDecks)
        {
            if (numberOfDecks <= 1)
                return new List<Deck>();

            var decks = new List<Deck>();

            for (var i = 1; i <= numberOfDecks; i++)
                decks.Add(new Deck());

            var counter = 0;
            while (!this.IsEmpty)
            {
                decks[counter].AddCard(this.Draw());
                counter = (counter + 1) % numberOfDecks;
            }

            return decks;
        }

        /// <summary>
        /// Places the deck onto the bottom of the provided deck returning the new deck
        /// </summary>
        /// <param name="finalDeck"></param>
        /// <returns></returns>
        public Deck PutInto(Deck finalDeck)
        {
            return finalDeck.AddCards(this.cards);
        }

    }
}
