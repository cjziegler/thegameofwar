﻿using System;
using System.ComponentModel;

namespace TheGameOfWar.Cards
{
    public class Card : IComparable<Card>
    {
        public Suit Suit { get; set; }

        public CardValue Value { get; set;}
        
        public int CompareTo(Card otherCard)
        {
            if (this.Value == otherCard.Value)
                return 0;

            return this.Value > otherCard.Value ? 1 : -1;
        }

        public string ToShortString()
        {
            if ((int)Value <= 10)
                return $"{(int)Value}{Suit.GetDescription()[0]}";

            return $"{Value.GetDescription()}{Suit.GetDescription()[0]}";
        }

        public override string ToString()
        {
            return $"{Value.ToString()} of {Suit.ToString()}s";
        }
    }

    public enum Suit
    {
        Heart,
        Diamond,
        Club,
        Spade
    }

    public enum CardValue
    {
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        [Description("J")]
        Jack = 11,
        [Description("Q")]
        Queen = 12,
        [Description("K")]
        King = 13,
        [Description("A")]
        Ace = 14
    }
}
