﻿using System;
using TheGameOfWar.Game;

namespace TheGameOfWar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Game of War!\r\n");

            new GameController().Play();
        }
    }
}
